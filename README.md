# Spring Boot, MySQL, JPA, Rest API

Build Restful CRUD API for a contact using Spring Boot, Mysql, JPA and Hibernate.
# spring-boot-crud-contact

## Steps to Setup
# default does not need config info mysql is already configured in mysql parameters on the server
**1. Clone the application**

# if running mysql locally then follow these steps
**2. Create Mysql database**
```bash
create database contact_inf
```
- run `src/main/resources/contact_inf.sql`
# 
**3. Change mysql username and password as per your installation**
+ open `src/main/resources/application.properties`
+ change `spring.datasource.username` and `spring.datasource.password` as per your mysql installation

**4. Run the app using maven**

```bash
mvn spring-boot:run
```
The app will start running at <http://localhost:9191>

## Explore Rest APIs

The app defines following CRUD APIs.

### Contact

| Method | Url | Description | Sample Valid Request Body |
| ------ | --- | ----------- | ------------------------- |
| GET    | /api/contact | Get all contact with paging | |
| GET    | /api/contact/{id} | Get contact by id | |
| POST   | /api/contact | Create new contact | [JSON](#contactCreate) |
| PUT    | /api/contact/{id} | Update a contact  | [JSON](#contactupdate) |
| DELETE | /api/contact/{id} | Delete a contact  | |


##### <a id="contactCreate">Create Contact -> /api/contact</a>
```json
{
  "first_name": "Nguyen",
  "last_name": "VAN A",
  "address": "648 binh thanh HCM",
  "phone": "0394679911",
  "postal": 700000
}
```


##### <a id="contactupdate">Update Todo -> /api/contact{id}</a>
```json
{
  "first_name": "Nguyen",
  "last_name": "VAN B",
  "address": "643/8 binh thanh HCM",
  "phone": "0395679922"
}
```