DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
                           `id` BIGINT(19) NOT NULL AUTO_INCREMENT,
                           `first_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
                           `last_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
                           `full_name` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
                           `address` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
                           `phone` VARCHAR(20) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
                           `postal` INT(10) NULL DEFAULT NULL,
                           `created_at` DATE NULL DEFAULT NULL,
                           `updated_at` DATE NULL DEFAULT NULL,
                           PRIMARY KEY (`id`) USING BTREE
)
    COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=8
;