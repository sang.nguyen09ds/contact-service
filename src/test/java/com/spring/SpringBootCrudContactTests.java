package com.spring;

import com.spring.model.Contact;
import com.spring.payload.request.ContactRequest;
import com.spring.repository.ContactRepository;
import com.spring.service.ContactService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class SpringBootCrudContactTests {

	@Mock
	private ContactRepository contactRepository; // Assume ContactRepository is a dependency of ContactService

	@InjectMocks
	private ContactService contactService;
	@Test
	public void testCreatContactSuccess(){
		// Arrange
		Contact contact = new Contact();
		contact.setFirst_name("NGUYEN");
		contact.setLast_name("VAN B");
		contact.setAddress("648 binh thanh HCM");
		contact.setPhone("0394679911");
		contact.setPostal(700000);
		when(contactRepository.save(any(Contact.class))).thenReturn(contact);
		// Act
		ContactRequest contactRequest = new ContactRequest();
		ResponseEntity<Contact> createContact = contactService.saveContact(contactRequest);
		// Assert
		assertNotNull(createContact);
		verify(contactRepository, times(1)).save(any(Contact.class));
	}


	@Test
	void contextLoads() {
	}

}
