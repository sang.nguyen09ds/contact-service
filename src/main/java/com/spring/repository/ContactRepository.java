package com.spring.repository;

import com.spring.model.Contact;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * @author SangNguyen
 * @since 09/06/2023
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact,Long> {

   @Query("SELECT e FROM Contact e WHERE e.full_name LIKE %:username% ORDER BY e.created_at")
   Page<Contact> findByName(@Param("username") String username, Pageable pageable);
}

