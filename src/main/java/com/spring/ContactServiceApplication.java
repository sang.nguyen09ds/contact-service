package com.spring;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.data.convert.Jsr310Converters;

@SpringBootApplication
@EntityScan(basePackageClasses = { ContactServiceApplication.class, Jsr310Converters.class })
public class ContactServiceApplication {
	private static final Logger logger = LoggerFactory.getLogger(ContactServiceApplication.class);
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(ContactServiceApplication.class);
		Environment env = app.run(args).getEnvironment();
		logger.info("==============================================================");
		logger.info("Application :  {} "," CONTACT-SERVICE");
		logger.info("Enviroment port:  {} ",env.getProperty("server.port"));
		logger.info("==============================================================");
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}
}
