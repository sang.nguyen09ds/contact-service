
package com.spring.payload.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactRequest {

	@Id
	@GeneratedValue
	private int id;
	@NotNull(message = "Please enter a valid first_name")
	private String first_name;

	@NotNull(message = "Please enter a valid last_name")
	private String last_name;

	@NotNull(message = "Please enter a valid address")
	private String address;

	@NotNull(message = "Please enter a valid phone")
	private String phone;

	private int postal;
}
