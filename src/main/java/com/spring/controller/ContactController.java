package com.spring.controller;

import com.spring.model.Contact;
import com.spring.payload.request.ApiResponse;
import com.spring.payload.request.ContactRequest;
import com.spring.payload.request.PagedResponse;
import com.spring.service.ContactService;
import com.spring.utils.AppConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author SangNguyen
 * @since 09/06/2023
 */
@RestController
public class ContactController {

    @Autowired
    private ContactService service;

    /**
     * @author SangNguyen
     * @since 09/06/2023
     * @see <strong>create new info contact</strong>
     * @param contactRequest
     * @return
     */
    @PostMapping(value = "/contact", produces = "application/json")
    public ResponseEntity<Contact> addContact(@Valid @RequestBody ContactRequest contactRequest) {
        System.out.println("out log");
        return service.saveContact(contactRequest);
    }

    /**
     * @author SangNguyen
     * @since 09/06/2023
     * @see <strong>get list contact support paging</strong>
     * @param username
     * @param page default 0
     * @param size default 20
     * @return
     */
    @GetMapping(value = "/contact", produces = "application/json")
    public ResponseEntity<PagedResponse<Contact>> getContactBy(
            @RequestParam(value = "username", required = false , defaultValue="") String username,
            @RequestParam(value = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
            @RequestParam(value = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size) {
        PagedResponse<Contact> response = service.getContactBy(username, page, size);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    /**
     * @author SangNguyen
     * @since 09/06/2023
     *  @see <strong>get detail contact</strong>
     * @param id
     * @return
     */
    @GetMapping(value = "contact/{id}", produces = "application/json")
    public ResponseEntity<Contact> getContact(@PathVariable(name = "id") Long id) {
        return service.getContact(id);
    }

    /**
     * @author SangNguyen
     * @since 09/06/2023
     * @see <strong>edit info contact by id </strong>
     * @param id
     * @param contactRequest
     * @return
     */
    @PutMapping(value = "/contact/{id}", produces = "application/json")
    public ResponseEntity<Contact> updateContact(@PathVariable(name = "id") Long id, @Valid @RequestBody ContactRequest contactRequest) {
        return service.updateContact(id, contactRequest);
    }

    /**
     * @author SangNguyen
     * @since 09/06/2023
     * @see <strong>delete info contact </strong>
     * @param id
     * @return
     */
    @DeleteMapping(value = "/contact/{id}", produces = "application/json")
    public ResponseEntity<ApiResponse> deleteContact(@PathVariable(name = "id") Long id) {
        return service.deleteContact(id);
    }

}
