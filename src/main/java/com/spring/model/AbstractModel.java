
package com.spring.model;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
abstract class AbstractModel  {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private int id;
    private Date createdAt;
    private Date updatedAt;
}
