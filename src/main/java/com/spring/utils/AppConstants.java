package com.spring.utils;

/**
 * @author SangNguyen
 * @since 09/06/2023
 */
public class AppConstants {
	public static final String DEFAULT_PAGE_NUMBER = "0";

	public static final String DEFAULT_PAGE_SIZE = "20";

	public static final String ID = "id";
	public static final String CONTACT = "contact";

	public static boolean isNullorZero(Integer i) {
		return 0 == (i == null ? 0 : i);
	}
}
