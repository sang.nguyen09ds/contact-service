package com.spring.service;

import com.spring.model.Contact;
import com.spring.payload.request.ApiResponse;
import com.spring.payload.request.ContactRequest;
import com.spring.payload.request.PagedResponse;
import org.springframework.http.ResponseEntity;

public interface ContactService {

	ResponseEntity<Contact> saveContact(ContactRequest contactRequest);

    PagedResponse<Contact> getContactBy(String username, Integer page, Integer size);

    ResponseEntity<Contact> getContact(Long id);

    ResponseEntity<Contact> updateContact(Long id, ContactRequest contactRequest);

    ResponseEntity<ApiResponse> deleteContact(Long id);
}
