package com.spring.service.impl;

import com.spring.exception.ContactException;
import com.spring.exception.ResourceNotFoundException;
import com.spring.model.Contact;
import com.spring.payload.request.ApiResponse;
import com.spring.payload.request.ContactRequest;
import com.spring.payload.request.PagedResponse;
import com.spring.repository.ContactRepository;
import com.spring.service.ContactService;
import com.spring.utils.AppConstants;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * @author SangNguyen
 * @since 09/06/2023
 */
@Service
public class ContactServiceImpl implements ContactService {
    private static final Logger log = LogManager.getLogger(ContactServiceImpl.class);
    private static final String CONTACT_STR = "contact";
    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public ResponseEntity<Contact> saveContact(ContactRequest contactRequest) {
       try {
           Contact request = modelMapper.map(contactRequest, Contact.class);
           String full_name = request.getFirst_name() + " " + request.getLast_name();
           request.setFull_name(full_name);
           request.setCreated_at(new Date());
           request.setUpdated_at(new Date());
           Contact result = contactRepository.save(request);
           return new ResponseEntity<>(result, HttpStatus.CREATED);
       } catch (Exception e){
           log.info(" Exception saveContact {}", e.getMessage());
       }
        throw new ContactException(HttpStatus.OK, " not found data");
    }

    @Override
    public PagedResponse<Contact> getContactBy(String username, Integer page, Integer size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Contact> posts = contactRepository.findByName(username, pageable);
            List<Contact> content = posts.getNumberOfElements() == 0 ? Collections.emptyList() : posts.getContent();
            return new PagedResponse<>(content, posts.getNumber(), posts.getSize(), posts.getTotalElements(),
                    posts.getTotalPages(), posts.isLast());

        } catch (Exception e) {
            log.info(" Exception saveContact {}", e.getMessage());
        }
        throw new ContactException(HttpStatus.OK, " not found data");
    }

    @Override
    public ResponseEntity<Contact> getContact(Long id) {
        try {
            Contact result = contactRepository.findById(id).orElse(null);
            if (result != null) {
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.info(" Exception {}", e.getMessage());
        }
        throw new ContactException(HttpStatus.OK, " not found data");
    }

    @Override
    public ResponseEntity<Contact> updateContact(Long id, ContactRequest contactRequest) {
        try {
            Contact update = contactRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(CONTACT_STR, AppConstants.ID, id));
            if (update != null) {
                update.setUpdated_at(new Date());
                update.setFirst_name(contactRequest.getFirst_name());
                update.setLast_name(contactRequest.getLast_name());
                update.setFull_name(contactRequest.getFirst_name() + "" + contactRequest.getLast_name());
                if (!contactRequest.getAddress().isEmpty()) {
                    update.setAddress(contactRequest.getAddress());
                }
                if (!contactRequest.getPhone().isEmpty()) {
                    update.setPhone(contactRequest.getPhone());
                }
                if (!AppConstants.isNullorZero(contactRequest.getPostal())) {
                    update.setPostal(contactRequest.getPostal());
                }
                Contact result = contactRepository.save(update);
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
        } catch (Exception e) {
            log.info(" Exception {}", e.getMessage());
        }
        throw new ContactException(HttpStatus.OK, " not found data");
    }

    @Override
    public ResponseEntity<ApiResponse> deleteContact(Long id) {
        try {
            contactRepository.deleteById(id);
            return new ResponseEntity<>(new ApiResponse(Boolean.TRUE, "You successfully deleted contact"), HttpStatus.OK);
        } catch (Exception e) {
            log.info(" Exception {}", e.getMessage());
        }
        throw new ContactException(HttpStatus.OK, " not found data");
    }
}
